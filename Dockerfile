FROM golang:1.14
WORKDIR /go/src/app
RUN go get github.com/vektra/mockery/.../
ENTRYPOINT [ "go", "run", "cmd/heureka-feeds/main.go"]
