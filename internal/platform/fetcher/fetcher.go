package fetcher

import (
	"io"
	"net/http"

	"github.com/pkg/errors"
)

var ErrBadResponseCode = errors.New("bad response code while fetching feed")

type HTTP struct {
	httpClient *http.Client
}

func New(httpClient *http.Client) *HTTP {
	return &HTTP{
		httpClient: httpClient,
	}
}

func (f *HTTP) Fetch(xmlURL string) (io.ReadCloser, error) {

	resp, err := f.httpClient.Get(xmlURL)
	if err != nil {
		return nil, errors.Wrap(err, "get xml feed")
	}

	if resp.StatusCode != http.StatusOK {
		return nil, ErrBadResponseCode
	}

	return resp.Body, nil
}
