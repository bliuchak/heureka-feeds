package decode

import "encoding/xml"

type Heureka struct {
	XMLName  xml.Name   `xml:"SHOP" json:"shop"`
	ShopItem []ShopItem `xml:"SHOPITEM" json:"shopitem"`
}

type ShopItem struct {
	ItemID            string      `xml:"ITEM_ID" json:"item_id"`
	ProductName       string      `xml:"PRODUCTNAME" json:"product_name"`
	Product           string      `xml:"PRODUCT" json:"product"`
	Description       string      `xml:"DESCRIPTION" json:"description"`
	URL               string      `xml:"URL" json:"url"`
	ImgURL            string      `xml:"IMGURL" json:"img_url"`
	ImgURLAlternative []string    `xml:"IMGURL_ALTERNATIVE,omitempty" json:"img_url_alternative,omitempty"`
	VideoURL          []string    `xml:"VIDEO_URL,omitempty" json:"video_url,omitempty"`
	PriceVAT          string      `xml:"PRICE_VAT" json:"price_vat"`
	VAT               string      `xml:"VAT" json:"vat"`
	ItemType          string      `xml:"ITEM_TYPE" json:"item_type"`
	Param             []Parameter `xml:"PARAM,omitempty" json:"param,omitempty"`
	Manufacturer      string      `xml:"MANUFACTURER" json:"manufacturer"`
	CategoryText      string      `xml:"CATEGORYTEXT" json:"category_text"`
	EAN               string      `xml:"EAN" json:"ean"`
	ISBN              string      `xml:"ISBN" json:"isbn"`
	HeurekaCPC        string      `xml:"HEUREKA_CPC,omitempty" json:"heureka_cpc,omitempty"`
	DeliveryDate      string      `xml:"DELIVERY_DATE" json:"delivery_date"`
	Delivery          []Delivery  `xml:"DELIVERY,omitempty" json:"delivery,omitempty"`
	ItemGroupID       string      `xml:"ITEMGROUP_ID" json:"item_group_id"`
	Accessory         []string    `xml:"ACCESSORY,omitempty" json:"accessory,omitempty"`
	Gift              []string    `xml:"GIFT,omitempty" json:"gift,omitempty"`
	// ProductNo is not described in https://sluzby.heureka.cz/napoveda/xml-feed/
	// but exists in "Ukázka základní podoby XML souboru" which is little bit strange.
	// Keep it here for consistency
	ProductNo string `xml:"PRODUCTNO" json:"productNo"`
	Dues      string `xml:"DUES,omitempty" json:"dues,omitempty"`
}

type Parameter struct {
	ParamName string `xml:"PARAM_NAME" json:"param_name"`
	Val       string `xml:"VAL" json:"val"`
}

type Delivery struct {
	DeliveryID       string `xml:"DELIVERY_ID" json:"delivery_id"`
	DeliveryPrice    string `xml:"DELIVERY_PRICE" json:"delivery_price"`
	DeliveryPriceCod string `xml:"DELIVERY_PRICE_COD" json:"delivery_price_cod"`
}
