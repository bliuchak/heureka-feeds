package decode

import (
	"encoding/xml"
	"io"

	"github.com/pkg/errors"
)

const targetElement = "SHOPITEM"

type XMLDecoder struct {
	ch    chan ShopItem
	errCh chan error
}

func New(ch chan ShopItem, errCh chan error) *XMLDecoder {
	return &XMLDecoder{
		ch:    ch,
		errCh: errCh,
	}
}

func (d *XMLDecoder) Decode(data io.ReadCloser) error {

	decoder := xml.NewDecoder(data)

	for {
		t, err := decoder.Token()
		if err != nil {
			if err != io.EOF {
				return errors.Wrap(err, "xml token")
			}
		}

		if t == nil {
			break
		}

		switch se := t.(type) {
		case xml.StartElement:

			if se.Name.Local == targetElement {
				var item ShopItem

				err = decoder.DecodeElement(&item, &se)

				if err != nil {
					return errors.Wrap(err, "decode xml element")
				}

				d.ch <- item

			}
		default:
		}
	}

	return nil
}
