package pubsub

import (
	"context"
	"encoding/json"

	"github.com/segmentio/kafka-go"
	"gitlab.com/bliuchak/heureka-feeds/internal/platform/decode"
)

//go:generate mockery --dir . --inpackage --testonly --name writer
type writer interface {
	WriteMessages(ctx context.Context, msgs ...kafka.Message) error
}

type Kafka struct {
	shopItems        writer
	shopItemsBidding writer
	errCh            chan error
}

func New(shopItems, shopItemsBidding writer, errCh chan error) *Kafka {
	return &Kafka{
		shopItems:        shopItems,
		shopItemsBidding: shopItemsBidding,
		errCh:            errCh,
	}
}

func (k *Kafka) WriteNoBidding(item ...decode.ShopItem) error {

	var msgs []kafka.Message

	for _, shopItem := range item {

		bytes, _ := json.Marshal(shopItem)

		msg := kafka.Message{
			Key:   []byte("shopItem"),
			Value: bytes,
		}

		msgs = append(msgs, msg)
	}

	return k.shopItems.WriteMessages(
		context.Background(),
		msgs...,
	)

}

func (k *Kafka) WriteBidding(item ...decode.ShopItem) error {

	var msgs []kafka.Message

	for _, shopItem := range item {

		bytes, _ := json.Marshal(shopItem)

		msg := kafka.Message{
			Key:   []byte("shopItem"),
			Value: bytes,
		}

		msgs = append(msgs, msg)
	}

	return k.shopItemsBidding.WriteMessages(
		context.Background(),
		msgs...,
	)

}
