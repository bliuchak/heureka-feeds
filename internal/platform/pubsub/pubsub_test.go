package pubsub

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/bliuchak/heureka-feeds/internal/platform/decode"
)

func TestKafka_WriteNoBidding(t *testing.T) {
	type input struct {
		err error
	}
	type output struct {
		err error
	}
	tests := []struct {
		name   string
		input  input
		output output
	}{
		{
			name: "should return nil error",
		},
		{
			name: "should return error on sending data to Kafka",
			input: input{
				err: assert.AnError,
			},
			output: output{
				err: assert.AnError,
			},
		},
	}

	for _, tt := range tests {

		t.Run(tt.name, func(t *testing.T) {

			item := decode.ShopItem{}

			errCh := make(chan error)
			defer close(errCh)

			shopItemsMock := new(mockWriter)
			defer shopItemsMock.AssertExpectations(t)

			shopItemsMock.On("WriteMessages", context.Background(), mock.AnythingOfType("kafka.Message")).
				Return(tt.input.err).
				Times(1)

			k := New(shopItemsMock, nil, errCh)

			err := k.WriteNoBidding(item)

			assert.Equal(t, tt.output.err, err)

		})
	}
}

func TestKafka_WriteBidding(t *testing.T) {
	type input struct {
		err error
	}
	type output struct {
		err error
	}
	tests := []struct {
		name   string
		input  input
		output output
	}{
		{
			name: "should return nil error",
		},
		{
			name: "should return error on sending data to Kafka",
			input: input{
				err: assert.AnError,
			},
			output: output{
				err: assert.AnError,
			},
		},
	}

	for _, tt := range tests {

		t.Run(tt.name, func(t *testing.T) {

			item := decode.ShopItem{}

			errCh := make(chan error)
			defer close(errCh)

			shopItemsBiddingMock := new(mockWriter)
			defer shopItemsBiddingMock.AssertExpectations(t)

			shopItemsBiddingMock.On("WriteMessages", context.Background(), mock.AnythingOfType("kafka.Message")).
				Return(tt.input.err).
				Times(1)

			k := New(nil, shopItemsBiddingMock, errCh)

			err := k.WriteBidding(item)

			assert.Equal(t, tt.output.err, err)

		})
	}
}
