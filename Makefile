current_dir = $(shell pwd)

# based on https://gist.github.com/prwhite/8168133
help:              ## show this help
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

build:             ## builds whole project
	docker network inspect lab >/dev/null 2>&1 || docker network create --driver bridge lab
	docker-compose build

run:               ## runs feeds and all necessary components
	docker-compose up feeds

kafdrop:           ## runs obsidiandynamics/kafdrop (Kafka Web UI)
	docker run -d -it -p 9000:9000 --network=lab -e KAFKA_BROKERCONNECT=kafka:9092 obsidiandynamics/kafdrop

golangci:          ## runs golangci-lint
	docker run --rm -v $(current_dir):/app -w /app golangci/golangci-lint:v1.27.0 golangci-lint run -v

hadolint:          ## runs hadolint
	docker run --rm -i hadolint/hadolint < Dockerfile

gogenerate:        ## runs go generate command
	docker-compose run --rm --entrypoint go feeds generate ./...

tests:             ## runs tests
	go test -race ./...

code-coverage:     ## runs tests and generate code coverage report in html
	go test -coverprofile=coverage.out ./... && go tool cover -html=coverage.out
