package main

import (
	"log"
	"net"
	"net/http"
	"time"
)

const httpPort = "3000"

func main() {
	log.Println("start serving fake feeds")
	fs := http.FileServer(http.Dir("./testdata"))
	err := http.ListenAndServe(net.JoinHostPort("", httpPort), durationMiddleware(fs))
	if err != nil {
		panic(err)
	}
}

func durationMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()
		log.Println("start serving", r.RequestURI)
		next.ServeHTTP(w, r)
		log.Println("stop serving", r.RequestURI, time.Since(start).String())
	})
}
