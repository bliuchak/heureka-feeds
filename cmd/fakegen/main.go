package main

import (
	"encoding/xml"
	"fmt"
	"math"
	"os"
	"strconv"
	"strings"

	"gitlab.com/bliuchak/heureka-feeds/internal/platform/decode"
	"syreclabs.com/go/faker"
)

func main() {
	var feedSize int

	// for the sake of simplicity I'm not going to use here any CLI package
	if len(os.Args) != 2 {
		fmt.Println("pls define feed size as 2nd cli argument")
		os.Exit(2)
	}

	feedSize, err := strconv.Atoi(os.Args[1])
	if err != nil {
		panic(err)
	}

	var shopItems []decode.ShopItem
	for i := 0; i < feedSize; i++ {
		shopItem := decode.ShopItem{
			ItemID:            faker.Lorem().Characters(36),
			ProductName:       faker.RandomString(200),
			Product:           faker.RandomString(255),
			Description:       faker.Lorem().Paragraph(5),
			URL:               faker.Internet().Url(),
			ImgURL:            faker.Avatar().String(),
			ImgURLAlternative: getimgURLAlternative(),
			VideoURL:          getVideoURL(),
			PriceVAT:          fmt.Sprintf("%f", faker.Commerce().Price()),
			VAT:               "20%",
			ItemType:          faker.Lorem().Word(),
			Param:             getParam(),
			Manufacturer:      faker.Lorem().Word(),
			CategoryText:      strings.Join(faker.Lorem().Words(3), " | "),
			EAN:               faker.Code().Ean13(),
			ISBN:              faker.Code().Isbn13(),
			HeurekaCPC:        getHeurekaCPC(),
			DeliveryDate:      faker.Number().Between(0, 35),
			Delivery:          getDelivery(),
			ItemGroupID:       faker.Lorem().Characters(36),
			Accessory:         getAccessory(),
			Gift:              getGift(),
			ProductNo:         faker.Lorem().Characters(36),
			Dues:              getDues(),
		}

		shopItems = append(shopItems, shopItem)
	}

	feed := &decode.Heureka{
		ShopItem: shopItems,
	}

	output, err := xml.MarshalIndent(feed, "", "  ")
	if err != nil {
		panic(err)
	}

	// append xml header
	output = []byte(xml.Header + string(output))

	os.Stdout.Write(output)
}

func getDelivery() []decode.Delivery {
	var delivery []decode.Delivery
	if math.Mod(float64(faker.Commerce().Price()), 2) > 1 {
		delivery = []decode.Delivery{
			{
				DeliveryID:       faker.Lorem().Word(),
				DeliveryPrice:    fmt.Sprintf("%f", faker.Commerce().Price()),
				DeliveryPriceCod: fmt.Sprintf("%f", faker.Commerce().Price()),
			},
		}
	}

	return delivery
}

func getimgURLAlternative() []string {
	var imgURLAlternative []string
	if math.Mod(float64(faker.Commerce().Price()), 2) > 1 {
		imgURLAlternative = []string{
			faker.Avatar().String(),
			faker.Avatar().String(),
		}
	}

	return imgURLAlternative
}

func getVideoURL() []string {
	var videoURL []string
	if math.Mod(float64(faker.Commerce().Price()), 2) > 1 {
		videoURL = []string{
			faker.Internet().Url(),
			faker.Internet().Url(),
		}
	}

	return videoURL
}

func getParam() []decode.Parameter {
	var param []decode.Parameter
	if math.Mod(float64(faker.Commerce().Price()), 2) > 1 {
		param = []decode.Parameter{
			{
				ParamName: "color",
				Val:       faker.Commerce().Color(),
			},
		}
	}

	return param
}

func getHeurekaCPC() string {
	var heurekaCPC string
	if math.Mod(float64(faker.Commerce().Price()), 2) > 1 {
		heurekaCPC = fmt.Sprintf("%f", faker.Commerce().Price())
	}

	return heurekaCPC
}

func getAccessory() []string {
	var accessory []string
	if math.Mod(float64(faker.Commerce().Price()), 2) > 1 {
		accessory = faker.Lorem().Words(2)
	}

	return accessory
}

func getGift() []string {
	var gift []string
	if math.Mod(float64(faker.Commerce().Price()), 2) > 1 {
		gift = faker.Lorem().Words(3)
	}

	return gift
}

func getDues() string {
	var dues string
	if math.Mod(float64(faker.Commerce().Price()), 2) > 1 {
		dues = fmt.Sprintf("%f", faker.Commerce().Price())
	}

	return dues
}
