package main

import (
	"context"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/kelseyhightower/envconfig"
	"github.com/rs/zerolog"
	"github.com/segmentio/kafka-go"
	"gitlab.com/bliuchak/heureka-feeds/cmd/heureka-feeds/feeds"
	"gitlab.com/bliuchak/heureka-feeds/internal/platform/decode"
	"gitlab.com/bliuchak/heureka-feeds/internal/platform/fetcher"
	"gitlab.com/bliuchak/heureka-feeds/internal/platform/pubsub"
)

type Config struct {
	Feeds             []string      `required:"true"`
	KafkaTopics       []string      `default:"shop_items,shop_items_bidding"`
	KafkaBrokers      []string      `default:"kafka:9092"`
	KafkaBatchTimeout time.Duration `default:"2ms"`
	// HTTPClientTimeout used to limit downloading time of xml feeds
	HTTPClientTimeout time.Duration `default:"10m"`
	PushGateway       string        `default:"pushgateway:9091"`
}

func main() {
	start := time.Now()

	log := zerolog.New(zerolog.ConsoleWriter{Out: os.Stderr}).With().Timestamp().Logger()
	log.Info().Msg("starting")

	var config Config

	err := envconfig.Process("heu", &config)
	if err != nil {
		log.Panic().Err(err).Msg("can't process envconfig")
	}

	singleBroker := strings.Join(config.KafkaBrokers, "")

	// make sure that leaders are ready to accept connections
	for _, topic := range config.KafkaTopics {
		_, err = kafka.DialLeader(context.Background(), "tcp", singleBroker, topic, 0)
		if err != nil {
			log.Panic().Err(err).Msg("dial kafka leader error")
		}
	}

	// creating necessary topic writers
	topicWriters := make(map[string]*kafka.Writer)
	for _, topic := range config.KafkaTopics {

		topicWriter := kafka.NewWriter(kafka.WriterConfig{
			Brokers:      config.KafkaBrokers,
			Topic:        topic,
			Balancer:     &kafka.LeastBytes{},
			BatchTimeout: config.KafkaBatchTimeout,
		})

		defer topicWriter.Close()

		topicWriters[topic] = topicWriter
	}

	itemsCh := make(chan decode.ShopItem)
	errCh := make(chan error)

	// handle errors from err channel
	go func() {
		for errItem := range errCh {
			log.Error().Err(errItem).Msg("feed processing error")
		}
	}()

	broker := pubsub.New(topicWriters["shop_items"], topicWriters["shop_items_bidding"], errCh)
	decoder := decode.New(itemsCh, errCh)
	httpClient := &http.Client{Timeout: config.HTTPClientTimeout}
	fetcher := fetcher.New(httpClient)

	app := feeds.New(log, itemsCh, errCh, broker, fetcher, decoder, config.PushGateway)
	app.Run(config.Feeds)

	log.Info().Str("duration", time.Since(start).String()).Msg("fullstop")
}
