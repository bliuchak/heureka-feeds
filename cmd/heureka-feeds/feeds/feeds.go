package feeds

import (
	"io"
	"sync"

	"github.com/rs/zerolog"
	"gitlab.com/bliuchak/heureka-feeds/internal/platform/decode"
)

type writer interface {
	WriteBidding(item ...decode.ShopItem) error
	WriteNoBidding(item ...decode.ShopItem) error
}

type fetcher interface {
	Fetch(xmlURL string) (io.ReadCloser, error)
}

type decoder interface {
	Decode(data io.ReadCloser) error
}

type Runner struct {
	logger zerolog.Logger

	itemsCh chan decode.ShopItem
	errCh   chan error
	broker  writer
	fetcher fetcher
	decoder decoder

	metrics *metrics
}

func New(logger zerolog.Logger,
	itemsCh chan decode.ShopItem,
	errCh chan error,
	broker writer,
	fetcher fetcher,
	decoder decoder,
	pushGateway string) *Runner {

	return &Runner{
		logger: logger,

		itemsCh: itemsCh,
		errCh:   errCh,
		broker:  broker,
		fetcher: fetcher,
		decoder: decoder,

		metrics: newPrometheus(logger, pushGateway),
	}
}

func (r *Runner) Run(feeds []string) {

	// worker pool for propagating parsed xml shopitem data into broker
	var pubsubWG sync.WaitGroup

	for i := 1; i <= 5; i++ {

		pubsubWG.Add(1)

		go func() {

			defer pubsubWG.Done()

			var noBidding []decode.ShopItem
			var bidding []decode.ShopItem

			for item := range r.itemsCh {

				// send batch messages into broker
				if item.HeurekaCPC == "" {
					noBidding = append(noBidding, item)
				} else {
					bidding = append(bidding, item)
				}

				if len(noBidding) == 1000 {
					err := r.broker.WriteNoBidding(noBidding...)
					if err == nil {
						noBidding = nil
					} else {
						r.errCh <- err
					}

				}

				if len(bidding) == 1000 {
					err := r.broker.WriteBidding(bidding...)
					if err == nil {
						bidding = nil
					} else {
						r.errCh <- err
					}
				}

			}

			// handle leftovers which are not suite into batch size
			if len(noBidding) > 0 {
				err := r.broker.WriteNoBidding(noBidding...)
				if err != nil {
					r.errCh <- err
				}
			}

			if len(bidding) > 0 {
				err := r.broker.WriteBidding(bidding...)
				if err != nil {
					r.errCh <- err
				}
			}

		}()
	}

	// worker pool for workers which fetch xml by http
	// and decode xml item
	var decoderWG sync.WaitGroup

	for _, feedURL := range feeds {

		decoderWG.Add(1)

		go func(feedURL string) {
			r.logger.Info().Str("feed_url", feedURL).Msg("start processing")

			defer decoderWG.Done()

			body, err := r.fetcher.Fetch(feedURL)

			// decode message in case fetch operation was successful
			if err == nil {
				err = r.decoder.Decode(body)

				// mark feed as processed successfully in case there aren't decode errors
				if err == nil {

					r.metrics.success(feedURL)
					r.logger.Info().Str("feed_url", feedURL).Msg("finish processing")
				}

			} else {

				// otherwise report an error and close this goroutine
				r.metrics.withError(feedURL)
				r.logger.Error().Err(err).Str("feed_url", feedURL).Msg("finish processing with error")

			}
		}(feedURL)
	}

	// wait until all decoder routines finish streaming xml data from feeds
	decoderWG.Wait()

	// close items channel after all decoders have finished
	close(r.itemsCh)

	// wait until all pubsub workers finish send parsed data
	pubsubWG.Wait()

	// close errCh since all the workers are down
	close(r.errCh)

}
