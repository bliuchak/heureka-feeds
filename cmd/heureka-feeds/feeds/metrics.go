package feeds

import (
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/push"
	"github.com/rs/zerolog"
)

type metrics struct {
	// url to prometheus push gateway
	pgURL  string
	logger zerolog.Logger

	registry *prometheus.Registry

	ok  *prometheus.GaugeVec
	err *prometheus.GaugeVec
}

func newPrometheus(logger zerolog.Logger, pgURL string) *metrics {
	m := &metrics{
		pgURL:    pgURL,
		registry: prometheus.NewRegistry(),

		ok: prometheus.NewGaugeVec(
			prometheus.GaugeOpts{
				Name: "feeds_ok",
				Help: "Unix timestamp when feeds job was finished successfully.",
			},
			[]string{"feed"},
		),
		err: prometheus.NewGaugeVec(
			prometheus.GaugeOpts{
				Name: "feeds_err",
				Help: "Unix timestamp when feeds job was finished with error.",
			},
			[]string{"feed"},
		),

		logger: logger,
	}

	m.registry.MustRegister(m.ok, m.err)

	return m
}

func (m *metrics) success(feed string) {
	m.ok.WithLabelValues(feed).Set(float64(time.Now().Unix()))
	m.push()
}

func (m *metrics) withError(feed string) {
	m.err.WithLabelValues(feed).Set(float64(time.Now().Unix()))
	m.push()
}

func (m *metrics) push() {
	err := push.New(m.pgURL, "feeds").Gatherer(m.registry).Push()
	if err != nil {
		m.logger.Error().Err(err).Msg("can't push metrics to pushgateway")
	}
}
