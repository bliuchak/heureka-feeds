module gitlab.com/bliuchak/heureka-feeds

go 1.14

require (
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/pkg/errors v0.9.1
	github.com/prometheus/client_golang v1.7.1
	github.com/rs/zerolog v1.19.0
	github.com/segmentio/kafka-go v0.3.7
	github.com/stretchr/testify v1.4.0
	golang.org/x/net v0.0.0-20200625001655-4c5254603344 // indirect
	golang.org/x/text v0.3.3 // indirect
	syreclabs.com/go/faker v1.2.2
)
