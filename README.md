# heureka-feeds

Fetch and Decode [Heureka XML](https://sluzby.heureka.cz/napoveda/xml-feed/) feeds. It also sends decoded data into Kafka topics:
- shop_items - data without bidding
- shop_items_bidding - data with bidding

Feeds processing process is paralleled. App uses XML stream processing to speed up the process of parsing.

Prometheus metrics - `http://0.0.0.0:9091/`

## Usage

0. Default [config](https://github.com/kelseyhightower/envconfig)

    ```bash
    Feeds             []string      `required:"true"`
    KafkaTopics       []string      `default:"shop_items,shop_items_bidding"`
    KafkaBrokers      []string      `default:"kafka:9092"`
    KafkaBatchTimeout time.Duration `default:"2ms"`
    HTTPClientTimeout time.Duration `default:"10m"`
    PushGateway       string        `default:"pushgateway:9091"`
    ```

1. Update `docker-compose.yaml` environment data for `feeds` (if necessary)

2. Build

    ```bash
    $ make build
    ```

2. Run

    ```bash
    $ make run
    ```

## Other usage

```bash
$ make
help:               show this help
build:              builds whole project
run:                runs feeds and all necessary components
kafdrop:            runs obsidiandynamics/kafdrop (Kafka Web UI)
golangci:           runs golangci-lint
hadolint:           runs hadolint
gogenerate:         runs go generate command
tests:              runs tests
code-coverage:      runs tests and generate code coverage report in html
```

## Other tools

### Kafdrop

[Kafdrop](https://github.com/obsidiandynamics/kafdrop) - Kafka Web UI `0.0.0.0:9000`

  ```bash
  $ make kafdrop
  ```

### Feed generator

Generates Heureka complient xml feed with specific size.

1. Build

    ```bash
    $ docker build --file Dockerfile.fakegen -t fakegen:0.0.0 .
    ```

2. Generate feed with `1000` shop items

    ```bash
    $ docker run --rm fakegen:0.0.0 1000 > testdata/1k.xml
    ```

### Feed server

Serves data (xml feeds) from `./testdata` folder. There is also middleware which measure requests duration.

1. Build

    ```bash
    $ docker build --rm --file Dockerfile.fakefeed --tag fakefeed:0.0.0 .
    ```

2. Run server

    ```bash
    $ docker run --rm --network=lab --publish 3000:3000 -v $(pwd)/testdata:/testdata --name=fakefeed fakefeed:0.0.0
    ```
